package com.huskey.restfulspringmvc5.repositories;

import com.huskey.restfulspringmvc5.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
