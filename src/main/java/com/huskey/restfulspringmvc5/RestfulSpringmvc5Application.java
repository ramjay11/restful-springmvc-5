package com.huskey.restfulspringmvc5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class RestfulSpringmvc5Application {

	public static void main(String[] args) {
		run(RestfulSpringmvc5Application.class, args);
	}

}
